package com.org.app;

import org.springframework.boot.SpringApplication;

public class TestFAuctionAppApplication {

    public static void main(String[] args) {
        SpringApplication.from(FAuctionAppApplication::main).with(TestcontainersConfiguration.class).run(args);
    }

}
