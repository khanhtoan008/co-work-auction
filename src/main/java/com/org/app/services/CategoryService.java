package com.org.app.services;

import com.org.app.dtos.categoryDto.CategoryDto;
import com.org.app.exceptions.ResourceNotFoundException;
import com.org.app.mapper.CategoryMapper;
import com.org.app.models.Category;
import com.org.app.repositories.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryRepository categoryRepository;
    private final CategoryMapper categoryMapper;

    public List<CategoryDto> getAllCategories() {
        List<Category> categoryList = categoryRepository.findAll();
        return categoryList.stream()
                .map(categoryMapper::toDto)
                .toList();
    }

    public CategoryDto getCategoryById(Long id) {

        return categoryMapper.toDto(findCategoryById(id));
    }

    public CategoryDto createCategory(CategoryDto request) {
        Category category = categoryMapper.toEntity(request);
        Category saveCategory = categoryRepository.save(category);

        return categoryMapper.toDto(saveCategory);
    }

    public void deleteCategory(Long id) {
        var addressToDelete = findCategoryById(id);
        categoryRepository.delete(addressToDelete);
    }

    public CategoryDto updateCategory(Long id, CategoryDto categoryRequestDto) {
        Category updateCategory = findCategoryById(id);

        categoryMapper.partialUpdate(categoryRequestDto, updateCategory);
        categoryRepository.save(updateCategory);
        return categoryMapper.toDto(updateCategory);
    }

    private Category findCategoryById(final Long id) {
        return categoryRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Category", "id", id));
    }
}
