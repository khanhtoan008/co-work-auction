package com.org.app.services;

import com.org.app.dtos.transactionDto.TransactionRequestDto;
import com.org.app.dtos.transactionDto.TransactionResponseDto;
import com.org.app.mapper.TransactionMapper;
import com.org.app.models.Transaction;
import com.org.app.exceptions.ResourceNotFoundException;
import com.org.app.repositories.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class TransactionService {
    private final TransactionRepository transactionRepository;
    private final TransactionMapper transactionMapper;
    public List<TransactionResponseDto> findAll() {

        List<Transaction> all = transactionRepository.findAll();
        return all.stream()
                .map(transactionMapper::toDto)
                .toList();
    }

    public TransactionResponseDto getTransactionById( Long id) {
        return transactionMapper.toDto(findTransactionById(id));
    }

    public TransactionResponseDto create(TransactionRequestDto request){
        Transaction transaction = transactionMapper.toEntity(request);
        Transaction saveTransaction = transactionRepository.save(transaction);

        return transactionMapper.toDto(saveTransaction);
    }

    public TransactionResponseDto updateTransaction(Long id,  TransactionRequestDto dto) {

        var transaction = findTransactionById(id);
        transactionMapper.partialUpdate(dto, transaction);

        var updatedTransaction = transactionRepository.save(transaction);

        return transactionMapper.toDto(updatedTransaction);
    }

    public void deleteTransaction(Long id) {
        var addressToDelete = findTransactionById(id);
        transactionRepository.delete(addressToDelete);
    }

    private Transaction findTransactionById(Long id) {
        return transactionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Transaction", "id", id));
    }
}
