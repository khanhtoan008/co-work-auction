package com.org.app.services;

import com.org.app.dtos.auctionDto.AuctionSessionRequestDto;
import com.org.app.dtos.auctionDto.AuctionSessionResponseDto;
import com.org.app.exceptions.ResourceNotFoundException;
import com.org.app.mapper.AuctionMapper;
import com.org.app.models.AuctionSession;
import com.org.app.repositories.AuctionSessionRepository;
import com.org.app.repositories.ProductRepository;
import com.org.app.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AuctionSessionService {
    private final AuctionSessionRepository auctionSessionRepository;
    private final ProductRepository productRepository;
    private final AuctionMapper auctionSessionMapper;
    private final UserRepository userRepository;


    public List<AuctionSessionResponseDto> getAllAuctionSessions() {

        List<AuctionSession> all = auctionSessionRepository.findAll();

        return all.stream()
                .map(auctionSessionMapper::toDto)
                .toList();
    }

    public AuctionSessionResponseDto getAuctionSessionById(Long id) {
        return auctionSessionMapper.toDto(findAuctionSessionById(id));
    }

    public AuctionSessionResponseDto createAuctionSession(AuctionSessionRequestDto auctionSession) {

        var seller = userRepository.findById(auctionSession.getSellersId())
                .orElseThrow(()-> new ResourceNotFoundException("Seller"," id " , auctionSession.getSellersId()));

        var product = productRepository.findById(auctionSession.getProductId())
                .orElseThrow(()-> new ResourceNotFoundException("Product"," id " , auctionSession.getProductId()));


        var create = auctionSessionMapper.toEntity(auctionSession);

        create.setSeller(seller);

        create.setProduct(product);
        var saveAuctionSession = auctionSessionRepository.save(create);

        return auctionSessionMapper.toDto(saveAuctionSession);
    }

    public AuctionSessionResponseDto updateAuctionSession(Long id, AuctionSessionRequestDto auctionSessionDetails) {
        AuctionSession auctionSession = findAuctionSessionById(id);
        auctionSession.setDuration(auctionSessionDetails.getDuration());

        auctionSessionMapper.partialUpdate(auctionSessionDetails, auctionSession);

        return auctionSessionMapper.toDto(auctionSession);
    }

    public void deleteAuctionSession(Long id) {
        AuctionSession auctionSession = findAuctionSessionById(id);
        auctionSessionRepository.delete(auctionSession);
    }
    private AuctionSession findAuctionSessionById(final Long id) {
        return auctionSessionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("AuctionSession", "id", id));
    }
}
