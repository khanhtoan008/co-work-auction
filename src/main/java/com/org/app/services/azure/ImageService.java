package com.org.app.services.azure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class ImageService {
    @Autowired
    private AzureBlobService azureBlobService;

    public List<String> uploadImages(List<MultipartFile> images) throws IOException {
        // Limit the number of images to 4
        if (images.size() > 4) {
            throw new IllegalArgumentException("You can upload a maximum of 4 images.");
        }

        return azureBlobService.uploadFiles(images);
    }


}
