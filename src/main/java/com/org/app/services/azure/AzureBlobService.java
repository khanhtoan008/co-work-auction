package com.org.app.services.azure;

import com.azure.storage.blob.*;

import com.azure.storage.blob.models.BlobHttpHeaders;
import com.azure.storage.blob.specialized.BlockBlobClient;
import com.azure.storage.common.StorageSharedKeyCredential;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class AzureBlobService {


    @Autowired
    private BlobServiceClient blobServiceClient;



    public List<String> uploadFiles(List<MultipartFile> files) throws IOException {
        String containerName = "product-image";
        BlobContainerClient containerClient = blobServiceClient.getBlobContainerClient(containerName);
        if (!containerClient.exists()) {
            containerClient.create();
        }

        return files.stream().map(file -> {
            String originalFilename = file.getOriginalFilename();
            String blobName = generateValidBlobName(originalFilename);
            BlobClient blobClient = containerClient.getBlobClient(blobName);
            try {
                blobClient.uploadWithResponse(file.getInputStream(), file.getSize(), null, new BlobHttpHeaders().setContentType(file.getContentType()), null, null, null, null, null);
                return blobClient.getBlobUrl();
            } catch (IOException e) {
                throw new RuntimeException("Failed to upload file: " + originalFilename, e);
            }
        }).collect(Collectors.toList());
    }

    public Boolean deleteImages(List<String> urls) throws IOException{

        BlobContainerClient containerClient = blobServiceClient.getBlobContainerClient("product-image");
        urls.forEach(s -> containerClient.getBlobClient(s.split("/")[s.split("/").length-1]).delete());
        return true;
    }

    private String generateValidBlobName(String originalFilename) {
        // Replace invalid characters with a valid substitute
        return UUID.randomUUID() +originalFilename.replaceAll("[^a-zA-Z0-9\\.\\-]", "_");
    }

}
