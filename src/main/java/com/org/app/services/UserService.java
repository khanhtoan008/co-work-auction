package com.org.app.services;

import com.org.app.dtos.userDto.UserRequestDto;
import com.org.app.dtos.userDto.UserResponseDto;
import com.org.app.exceptions.ResourceNotFoundException;
import com.org.app.mapper.UserMapper;
import com.org.app.models.Role;
import com.org.app.models.User;
import com.org.app.repositories.RoleRepository;
import com.org.app.repositories.UserRepository;
import com.org.app.security.SecurityUser;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;

    public UserResponseDto getUserById(Long id) {
        return userMapper.toDto(findUserById(id));
    }

    public List<UserResponseDto> findAllUsers() {
        List<User> allUsers = userRepository.findAll();

        return allUsers.stream()
                .map(userMapper::toDto)
                .toList();
    }


    public UserResponseDto createUser(UserRequestDto userRequestDto) {
        final String defaultRole = "USER";
        User user = userMapper.toEntity(userRequestDto);

        User saveUser = userRepository.save(user);
        saveUser.setPassword(passwordEncoder.encode(user.getPassword()));

        Role roleUser = roleRepository.findByName(defaultRole)
                .orElseThrow();

        saveUser.setRole(roleUser);

        return userMapper.toDto(saveUser);
    }


    public void deleteUser(Long id) {
        User user = findUserById(id);
        userRepository.delete(user);
    }


    public UserResponseDto updateUser(Long id, UserRequestDto userRequestDto) {
        User user = findUserById(id);
        userMapper.partialUpdate(userRequestDto, user);
        userRepository.save(user);

        return userMapper.toDto(user);

    }

    private User findUserById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", id));
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        return userRepository.getUserByEmail(username).map(SecurityUser::new)
                .orElseThrow();
    }
}
