package com.org.app.services;

import com.org.app.dtos.productDto.ProductRequestDto;
import com.org.app.dtos.productDto.ProductResponseDto;
import com.org.app.exceptions.ResourceNotFoundException;
import com.org.app.mapper.ProductMapper;
import com.org.app.models.*;
import com.org.app.repositories.CategoryRepository;
import com.org.app.repositories.ProductRepository;
import com.org.app.repositories.UserRepository;
import com.org.app.services.azure.AzureBlobService;
import com.org.app.services.azure.ImageService;
import com.org.app.utils.SecurityUtils;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

import org.springframework.http.HttpStatus;
import org.springframework.security.authorization.AuthorizationDeniedException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class ProductService {

    private final ProductRepository productRepository;
    private final UserRepository userRepository;
    private final CategoryRepository categoryRepository;
    private final ProductMapper productMapper;
    private final ImageService imageService;
    private final AzureBlobService azureBlobStorageService;

    public ProductResponseDto createProduct(ProductRequestDto productRequestDTO) {


        Product product = productMapper.toEntity(productRequestDTO);
        var currentUser = SecurityUtils.getCurrentUser();
        Category category = categoryRepository.findById(productRequestDTO.getCategoryId())
                .orElseThrow(() -> new ResourceNotFoundException("Category","id " , productRequestDTO.getCategoryId()));

        product.setOwner(currentUser);
        product.setCategory(category);

        Product savedProduct = productRepository.save(product);
        ProductResponseDto dto = productMapper.toDto(savedProduct);
        dto.setImageUrls(savedProduct.getImageUrls());
        return dto;
    }
    
    

    public ProductResponseDto updateProduct(Long id, ProductRequestDto productRequestDTO) {
        var product = findProductById(id);

        var category = categoryRepository.findById(productRequestDTO.getCategoryId())
                .orElseThrow(() -> new ResourceNotFoundException("Category","id " , productRequestDTO.getCategoryId()));

        productMapper.partialUpdate(productRequestDTO, product);
        product.setCategory(category);

        Product updatedProduct = productRepository.save(product);
        return productMapper.toDto(updatedProduct);
    }


    public void deleteProduct(Long id) throws IOException {

        Product product = findProductById(id);
        boolean currentUserId = SecurityUtils.isCurrentUser(product.getOwner().getId());


        if (currentUserId) {
            azureBlobStorageService.deleteImages(product.getImageUrls());
            productRepository.delete(product);
        }
        else
            throw new AuthorizationDeniedException("Not authorize to perform this action.", () -> false);
    }


    public ProductResponseDto getProductById(Long id) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Product","id ", id));
        return productMapper.toDto(product);
    }


    public List<ProductResponseDto> getAllProducts() {
        List<Product> products = productRepository.findAll();
        return products.stream()
                .map(productMapper::toDto)
                .collect(Collectors.toList());
    }

    private Product findProductById(final Long id) {
        return productRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Product", "id", id));
    }

    private List<Product> findByOwner(){
        Long currentUserId = SecurityUtils.getCurrentUserId();
        return productRepository.findProductsByOwner_Id(currentUserId);
    }

    public List<ProductResponseDto> getProductsByCurrentUser() {

        List<Product> products = findByOwner();

        return products.stream()
                .map(productMapper::toDto)
                .collect(Collectors.toList());
    }

    public void addImagesToProduct(Long productId, List<String> imageUrls) {
        Product product = findProductById(productId);

        List<String> existingImages = product.getImageUrls();
        if (existingImages == null) {
            existingImages = new ArrayList<>();
        }
        existingImages.addAll(imageUrls);
        product.setImageUrls(existingImages);
        productRepository.save(product);
    }

    // addd
}
