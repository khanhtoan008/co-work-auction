//package com.org.app.services;
//
//import com.org.app.exceptions.ResourceNotFoundException;
//import com.org.app.models.AppraisalReport;
//import com.org.app.repositories.AppraisalReportRepository;
//import lombok.RequiredArgsConstructor;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
//@Service
//@RequiredArgsConstructor
//public class AppraisalReportService {
//    private final AppraisalReportRepository appraisalReportRepository;
//    private final AppraisalReportMapper appraisalReportMapper;
//
//
//    public List<AppraisalReportResponseDto> getAllAppraisalReports() {
//        List<AppraisalReport> categoryList = appraisalReportRepository.findAll();
//        return categoryList.stream()
//                .map(appraisalReportMapper::toDto)
//                .toList();
//    }
//
//    public AppraisalReportResponseDto getReportById(Long id) {
//
//        return appraisalReportMapper.toDto(findAppraisalReportById(id));
//    }
//
//    public AppraisalReportResponseDto create(AppraisalReportRequestDto requestDto) {
//        AppraisalReport appraisalReport = appraisalReportMapper.toEntity(requestDto);
//        AppraisalReport savedReport = appraisalReportRepository.save(appraisalReport);
//        return appraisalReportMapper.toDto(savedReport);
//    }
//
//    public AppraisalReportResponseDto update(Long id, AppraisalReportRequestDto requestDto) {
//        AppraisalReport existingReport = appraisalReportRepository.findById(id)
//                .orElseThrow(() -> new ResourceNotFoundException("AppraisalReport", "id", id));
//        appraisalReportMapper.partialUpdate(requestDto, existingReport);
//        AppraisalReport updatedReport = appraisalReportRepository.save(existingReport);
//        return appraisalReportMapper.toDto(updatedReport);
//    }
//
//    public void deleteReport(Long id) {
//        var addressToDelete = findAppraisalReportById(id);
//        appraisalReportRepository.delete(addressToDelete);
//    }
//
//    private AppraisalReport findAppraisalReportById(final Long id) {
//        return appraisalReportRepository.findById(id)
//                .orElseThrow(() -> new ResourceNotFoundException("AppraisalReport", "id", id));
//    }
//
//}
