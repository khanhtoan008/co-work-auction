package com.org.app.services;

import com.org.app.dtos.shippingAddressDto.ShippingAddressDto;
import com.org.app.exceptions.ResourceNotFoundException;
import com.org.app.mapper.ShippingAddressMapper;
import com.org.app.models.ShippingAddress;
import com.org.app.repositories.ShippingAddressRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class ShippingAddressService {

    private final ShippingAddressRepository shippingAddressRepository;
    private final ShippingAddressMapper shippingAddressMapper;


    public List<ShippingAddressDto> findAll() {

        List<ShippingAddress> all = shippingAddressRepository.findAll();

        return all.stream()
                .map(shippingAddressMapper::toDto)
                .toList();
    }

    public ShippingAddressDto create(ShippingAddressDto request){
        ShippingAddress shippingAddress = shippingAddressMapper.toEntity(request);
        ShippingAddress saveAddress = shippingAddressRepository.save(shippingAddress);

        return shippingAddressMapper.toDto(saveAddress);
    }

    public ShippingAddressDto updateShippingAddress( Long id,  ShippingAddressDto dto) {

         var existingAddress = findShippingAddressById(id);
        shippingAddressMapper.update(dto, existingAddress);

         var updatedAddress = shippingAddressRepository.save(existingAddress);

        return shippingAddressMapper.toDto(updatedAddress);
    }

    public ShippingAddressDto getShippingAddressById( Long id) {
        return shippingAddressMapper.toDto(findShippingAddressById(id));
    }
    private ShippingAddress findShippingAddressById( Long id) {
        return shippingAddressRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Address", "id", id));
    }

    public void deleteShippingAddressById( Long id) {

         var addressToDelete = findShippingAddressById(id);
        shippingAddressRepository.delete(addressToDelete);
    }

}
