package com.org.app.exceptions;

import com.org.app.constants.ApiConstants;
import com.org.app.dtos.response.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.authorization.AuthorizationDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@RestControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiResponse<HashMap<String, String>> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        HashMap<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        return new ApiResponse<>(HttpStatus.BAD_REQUEST.value(), ApiConstants.BAD_REQUEST, errors);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ApiResponse<List<String>> handleNotFoundException(ResourceNotFoundException ex) {
        return new ApiResponse<>(
                HttpStatus.NOT_FOUND.value(),
                ApiConstants.RESOURCE_NOT_FOUND,
                Collections.singletonList(ex.getMessage()));
    }

    @ExceptionHandler(AuthorizationDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ApiResponse<List<String>> handleAuthorizationDeniedException(AuthorizationDeniedException ex) {
        return new ApiResponse<>(
                HttpStatus.FORBIDDEN.value(),
                ApiConstants.FORBIDDEN,
                Collections.singletonList(ex.getMessage())
        );
    }

}
