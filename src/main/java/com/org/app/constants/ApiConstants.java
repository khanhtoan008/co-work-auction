package com.org.app.constants;

public final class ApiConstants {
    public static final String SUCCESS = "SUCCESS";
    public static final String RESOURCE_CREATED = "RESOURCE_CREATED";
    public static final String RESOURCE_UPDATED = "RESOURCE_UPDATED";
    public static final String RESOURCE_DELETED = "RESOURCE_DELETED";
    public static final String RESOURCE_FOUND = "RESOURCE_FOUND";
    public static final String RESOURCE_LIST_RETRIEVED = "RESOURCE_LIST_RETRIEVED";

    public static final String ERROR = "ERROR";
    public static final String RESOURCE_NOT_FOUND = "RESOURCE_NOT_FOUND";
    public static final String BAD_REQUEST = "BAD_REQUEST";
    public static final String INVALID_INPUT = "INVALID_INPUT";
    public static final String INTERNAL_SERVER_ERROR = "INTERNAL_SERVER_ERROR";
    public static final String UNAUTHORIZED = "UNAUTHORIZED";
    public static final String FORBIDDEN = "FORBIDDEN";
    public static final String METHOD_NOT_ALLOWED = "METHOD_NOT_ALLOWED";
    public static final String NOT_ACCEPTABLE = "NOT_ACCEPTABLE";
    public static final String CONFLICT = "CONFLICT";

    public static final String VALIDATION_ERROR = "VALIDATION_ERROR";

    public static final String AUTHENTICATION_FAILED = "AUTHENTICATION_FAILED";
    public static final String ACCESS_DENIED = "ACCESS_DENIED";
}
