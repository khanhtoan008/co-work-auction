package com.org.app.models;


import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "transactions")
public class Transaction extends DistributedEntity {
    public enum TransactionType {
        NAP_TIEN,
        RUT_TIEN
    }
    public enum TransactionStatus {
        CHO_XU_LY,
        THANH_CONG,
        THAT_BAI
    }

    @ManyToOne
    private Wallet wallet;

    @Enumerated(EnumType.STRING)
    private TransactionType type;

    private Float amount;
    private Date time;

    @Enumerated(EnumType.STRING)
    private TransactionStatus status;

    @OneToOne
    private Order order;
}
