package com.org.app.models;

import jakarta.persistence.*;
import lombok.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "users")
public class User extends DistributedEntity {

    private String name;
    private String email;
    private String password;

    @Column(unique = true)
    private String CCCD;

    private Float rate;

    @OneToOne(mappedBy = "user")
    private Wallet wallet;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn
    private Set<ShippingAddress> shippingAddress = new HashSet<>();

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Role role;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Bid> bid;

    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
    private List<Product> productStorage;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn
    private Set<Order> order = new HashSet<>();

    @OneToMany(mappedBy = "seller", fetch = FetchType.LAZY)
    private List<AuctionSession> auctionSessions;

//    @ManyToMany(mappedBy = "buyers", fetch = FetchType.LAZY)
//    private List<AuctionSession> joinedAuctionSessions;

    private void addWallet(Wallet wallet) {
        this.wallet = wallet;
        wallet.setUser(this);
    }

    private void addProduct(Product product) {
        this.productStorage.add(product);
        product.setOwner(this);
    }

}