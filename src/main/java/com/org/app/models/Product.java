package com.org.app.models;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "products")
public class Product extends DistributedEntity {
    private String name;
    private String description;
    private Float buyoutPrice;
    private Float startPrice;

    @ElementCollection
    @CollectionTable(name = "product_images", joinColumns = @JoinColumn(name = "product_id"))
    @Column(name = "image_url")
    private List<String> imageUrls;

    @ManyToOne(fetch = FetchType.LAZY)
    private User owner;

    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;

    @OneToOne(mappedBy = "product", cascade = CascadeType.ALL)
    private AuctionSession auctionSession;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private AppraisalReport appraisalReports;

    public boolean isAuctionActive() {
        return auctionSession != null && auctionSession.isActive();
    }
}
