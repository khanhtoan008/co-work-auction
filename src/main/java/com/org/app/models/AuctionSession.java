package com.org.app.models;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "auction_sessions")
public class AuctionSession extends DistributedEntity {


    private Float currentPrice;
    private String duration; // tính bằng giờ
    private Date startTime;

    @ManyToOne
    private User seller;

    @OneToOne
    private User winner;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Product product;

    public boolean isActive() {
        Date now = new Date();
        long auctionEndTime = startTime.getTime() + Long.parseLong(duration) * 3600 * 1000;
        return now.after(startTime) && now.before(new Date(auctionEndTime));
    }
}
