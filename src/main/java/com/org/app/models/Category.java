package com.org.app.models;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "categories")
public class Category extends DistributedEntity {

    @OneToMany
    @JoinColumn
    private List<Product> products;

    private String name;
    private String description;
}

