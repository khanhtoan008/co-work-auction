package com.org.app.models;

import jakarta.persistence.*;
import lombok.*;


import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "appraisal_reports")
public class AppraisalReport extends DistributedEntity {

    @OneToOne(mappedBy = "appraisalReports")
    private Product requestProduct;

    @OneToMany
    @JoinColumn
    private List<User> requestAppraisal; //List Người dùng gửi thẩm định

    @ManyToOne
    private User appraisal;  //Người đánh giá thẩm định

    private String detail;
    private Boolean status;

}
