package com.org.app.models;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "wallets")
@ToString
public class Wallet extends DistributedEntity {

    private Float balance;

    @OneToOne(fetch = FetchType.LAZY)
    private User user;
}
