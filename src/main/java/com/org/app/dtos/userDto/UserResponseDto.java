package com.org.app.dtos.userDto;

import com.org.app.dtos.productDto.ProductResponseDto;
import com.org.app.models.Product;
import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;
import lombok.Value;

import java.io.Serializable;
import java.util.List;

/**
 * DTO for {@link com.org.app.models.User}
 */
@Value
@Builder
public class UserResponseDto {

    Long id;
    @NotBlank
    String name;
    @NotBlank
    String email;
    Float rate;
    String roleName;

    List<ProductResponseDto> productStorage;

}