package com.org.app.dtos.userDto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Builder;
import lombok.Data;
import lombok.Value;

import java.io.Serializable;

/**
 * DTO for {@link com.org.app.models.User}
 */
@Value
@Builder
public class UserRequestDto {

    @NotBlank
    String name;

    @Email
    @NotBlank
    String email;

    @NotBlank
    String password;

    @NotBlank
    String CCCD;

    Float rate;
}