package com.org.app.dtos.userDto;

import lombok.Builder;
import lombok.Value;

import java.io.Serializable;

/**
 * DTO for {@link com.org.app.models.User}
 */
@Value
@Builder
public class UserAuctionDto implements Serializable {
    Long id;
    String name;
    String email;
}