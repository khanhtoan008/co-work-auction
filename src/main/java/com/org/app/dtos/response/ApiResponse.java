package com.org.app.dtos.response;

import java.io.Serializable;

public record ApiResponse<T>(int status, String message, T payload) implements Serializable {}

