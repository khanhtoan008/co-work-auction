package com.org.app.dtos.categoryDto;

import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Value;

import java.io.Serializable;

/**
 * DTO for {@link com.org.app.models.Category}
 */
@Value
@Builder
public class CategoryDto implements Serializable {
    Long id;
    @NotBlank
    String name;
    @NotBlank
    String description;
}