package com.org.app.dtos.shippingAddressDto;

import jakarta.validation.constraints.NotBlank;
import lombok.Value;

import java.io.Serializable;

/**
 * DTO for {@link com.org.app.models.ShippingAddress}
 */
@Value
public class ShippingAddressDto implements Serializable {
    @NotBlank
    String street;
    @NotBlank
    String ward;
    @NotBlank
    String district;
    @NotBlank
    String city;
}