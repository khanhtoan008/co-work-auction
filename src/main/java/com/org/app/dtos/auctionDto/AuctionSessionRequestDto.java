package com.org.app.dtos.auctionDto;

import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Value;

import java.io.Serializable;
import java.util.Date;

/**
 * DTO for {@link com.org.app.models.AuctionSession}
 */
@Value
@Builder
public class AuctionSessionRequestDto{

    Date startTime;

    @NotBlank
    String duration;

    Long sellersId;
    Long productId;
}


