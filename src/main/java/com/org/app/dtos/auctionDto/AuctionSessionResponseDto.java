package com.org.app.dtos.auctionDto;

import com.org.app.dtos.productDto.ProductResponseDto;
import com.org.app.dtos.userDto.UserAuctionDto;
import com.org.app.dtos.userDto.UserResponseDto;
import com.org.app.models.Product;
import lombok.Builder;
import lombok.Value;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * DTO for {@link com.org.app.models.AuctionSession}
 */
@Value
@Builder
public class AuctionSessionResponseDto implements Serializable {

    Long id;
    Timestamp createdTime;
    Timestamp updatedTime;
    Float currentPrice;
    String duration;

    List<UserAuctionDto> buyers;

    UserAuctionDto seller;

    UserAuctionDto winner;

    ProductResponseDto product;

    Date startTime;
}