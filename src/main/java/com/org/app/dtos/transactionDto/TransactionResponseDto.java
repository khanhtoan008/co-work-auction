package com.org.app.dtos.transactionDto;

import com.org.app.models.Transaction;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * DTO for {@link com.org.app.models.Transaction}
 */
public record TransactionResponseDto(Long id, Timestamp createdTime, Timestamp updatedTime,
                                     Transaction.TransactionType type, Float amount, Date time,
                                     Transaction.TransactionStatus status, Long orderId, Timestamp orderCreatedTime,
                                     Timestamp orderUpdatedTime, String orderShippingCode, int orderTotal,
                                     Boolean orderStatus) implements Serializable {
}
