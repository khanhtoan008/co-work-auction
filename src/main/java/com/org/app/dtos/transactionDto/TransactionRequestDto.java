package com.org.app.dtos.transactionDto;

import com.org.app.models.Transaction;

import java.io.Serializable;
import java.util.Date;

/**
 * DTO for {@link com.org.app.models.Transaction}
 */
public record TransactionRequestDto(Long walletUserId, Transaction.TransactionType type, Float amount, Date time,
                                    Transaction.TransactionStatus status, String orderShippingCode, int orderTotal,
                                    Boolean orderStatus) implements Serializable {
}