package com.org.app.dtos.productDto;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class ProductDto {

    String name;
    String description;

}
