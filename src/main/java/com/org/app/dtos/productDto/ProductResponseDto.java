package com.org.app.dtos.productDto;

import com.org.app.dtos.categoryDto.CategoryDto;
import com.org.app.dtos.userDto.UserAuctionDto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.Value;

import java.util.List;

/**
 * DTO for {@link com.org.app.models.Product}
 */
@Data
@Builder
public class ProductResponseDto {

    Long id;

    @NotBlank
    String name;


    List<String> imageUrls;

    @NotBlank
    String description;

    @NotNull
    Float buyoutPrice;

    @NotNull
    Float startPrice;

    UserAuctionDto owner;

    CategoryDto category;

}