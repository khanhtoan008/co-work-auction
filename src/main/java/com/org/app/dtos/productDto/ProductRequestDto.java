package com.org.app.dtos.productDto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Builder;
import lombok.Data;
import lombok.Value;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * DTO for {@link com.org.app.models.Product}
 */
@Data
@Builder
public class ProductRequestDto {
    @NotBlank
    String name;

    @NotBlank
    String description;

    @NotNull
    Float buyoutPrice;

    @NotNull
    Float startPrice;

//    @NotNull
//    Long ownerId;

    @NotNull
    Long categoryId;
}