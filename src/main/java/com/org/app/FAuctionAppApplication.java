package com.org.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FAuctionAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(FAuctionAppApplication.class, args);
    }

}
