package com.org.app.mapper;

import com.org.app.dtos.transactionDto.TransactionRequestDto;
import com.org.app.dtos.transactionDto.TransactionResponseDto;
import com.org.app.models.Transaction;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public interface TransactionMapper {
    Transaction toEntity(TransactionRequestDto transactionRequestDto);

    TransactionResponseDto toDto(Transaction transaction);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Transaction partialUpdate(TransactionRequestDto transactionRequestDto, @MappingTarget Transaction transaction);
}