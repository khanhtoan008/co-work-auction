package com.org.app.mapper;

import com.org.app.dtos.auctionDto.AuctionSessionRequestDto;
import com.org.app.dtos.auctionDto.AuctionSessionResponseDto;
import com.org.app.models.AuctionSession;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING, uses = {ProductMapper.class, UserMapper.class})
public interface AuctionMapper {


    @Mapping(target = "product", source = "product")
    AuctionSessionResponseDto toDto(AuctionSession auctionSession);

    AuctionSession toEntity(AuctionSessionRequestDto auctionSessionRequestDTO);

    AuctionSession toResponseDTO(AuctionSession auctionSession);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    AuctionSession partialUpdate(AuctionSessionRequestDto auctionSessionRequestDto, @MappingTarget AuctionSession auctionSession);
}
