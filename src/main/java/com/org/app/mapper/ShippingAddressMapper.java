package com.org.app.mapper;

import com.org.app.dtos.shippingAddressDto.ShippingAddressDto;
import com.org.app.models.ShippingAddress;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public interface ShippingAddressMapper {
    ShippingAddress toEntity(ShippingAddressDto shippingAddressDto);

    ShippingAddressDto toDto(ShippingAddress shippingAddress);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void partialUpdate(ShippingAddressDto shippingAddressDto, @MappingTarget ShippingAddress shippingAddress);

    void update(ShippingAddressDto dto, @MappingTarget ShippingAddress entity);
}