package com.org.app.mapper;

import com.org.app.dtos.productDto.ProductRequestDto;
import com.org.app.dtos.productDto.ProductResponseDto;
import com.org.app.dtos.userDto.UserRequestDto;
import com.org.app.models.Product;
import com.org.app.models.User;
import com.org.app.services.azure.AzureBlobService;
import org.mapstruct.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Mapper(componentModel = "spring", uses = {UserMapper.class, CategoryMapper.class})
public interface ProductMapper {

    AzureBlobService azureBlobService = new AzureBlobService();


    Product toEntity(ProductRequestDto productRequestDTO);


    @Mapping(target = "category", source = "category")
    @Mapping(target = "owner", source = "owner")
    ProductResponseDto toDto(Product product);

    @Named("mapMultipartFileListToStringList")
    default List<String> mapMultipartFileListToStringList(List<MultipartFile> files) throws IOException {
        return azureBlobService.uploadFiles(files);
    }

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "imageUrls", ignore = true)
    void partialUpdate(ProductRequestDto productRequestDto, @MappingTarget Product product);

}
