package com.org.app.mapper;

import com.org.app.dtos.userDto.UserAuctionDto;
import com.org.app.dtos.userDto.UserRequestDto;
import com.org.app.dtos.userDto.UserResponseDto;
import com.org.app.models.User;
import org.mapstruct.*;


@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING, uses = {ProductMapper.class})
public interface UserMapper {
    User toEntity(UserRequestDto userRequestDto);

    @Mapping(source = "role.name", target = "roleName")
    @Mapping(source = "productStorage", target = "productStorage")
    UserResponseDto toDto(User user);

    void update(UserRequestDto requestDto, @MappingTarget User user);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void partialUpdate(UserRequestDto userRequestDto, @MappingTarget User user);
}
