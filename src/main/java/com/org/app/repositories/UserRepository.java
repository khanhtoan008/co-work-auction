package com.org.app.repositories;

import com.org.app.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;


public interface UserRepository extends JpaRepository<User, Long> {
//    Optional<User> findUserByEmail(String email);

    @Query("select u from User u where u.email = ?1")
    Optional<User> getUserByEmail(String email);
}