package com.org.app.repositories;

import com.org.app.models.AuctionSession;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuctionSessionRepository extends JpaRepository<AuctionSession, Long> {
}