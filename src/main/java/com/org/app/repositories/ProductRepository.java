package com.org.app.repositories;

import com.org.app.models.Product;
import com.org.app.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query("select p from Product p where p.owner.id = ?1")
    List<Product> findProductsByOwner_Id(Long id);

}