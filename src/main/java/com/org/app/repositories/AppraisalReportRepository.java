package com.org.app.repositories;

import com.org.app.models.AppraisalReport;
import com.org.app.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppraisalReportRepository extends JpaRepository<AppraisalReport, Long> {

}