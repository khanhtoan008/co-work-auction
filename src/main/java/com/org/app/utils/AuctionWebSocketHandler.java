//package com.org.app.utils;
//
//import org.springframework.web.socket.CloseStatus;
//import org.springframework.web.socket.TextMessage;
//import org.springframework.web.socket.WebSocketSession;
//import org.springframework.web.socket.handler.TextWebSocketHandler;
//
//import java.util.Collections;
//import java.util.HashSet;
//import java.util.Set;
//
//public class AuctionWebSocketHandler extends TextWebSocketHandler {
//    private Set<WebSocketSession> sessions = Collections.synchronizedSet(new HashSet<>());
//
//    @Override
//    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
//        sessions.add(session);
//        // Notify other users about new participant
//        for (WebSocketSession s : sessions) {
//            s.sendMessage(new TextMessage("A new participant has joined the auction"));
//        }
//    }
//
//    @Override
//    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
//        sessions.remove(session);
//        // Notify other users about participant leaving
//        for (WebSocketSession s : sessions) {
//            s.sendMessage(new TextMessage("A participant has left the auction"));
//        }
//    }
//
//    @Override
//    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
//        // Handle incoming messages
//        for (WebSocketSession s : sessions) {
//            s.sendMessage(message);
//        }
//    }
//
//
//}
