//package com.org.app.controllers;
//
//import com.org.app.constants.ApiConstants;
//import com.org.app.dtos.response.ApiResponse;
//import com.org.app.services.AppraisalReportService;
//import jakarta.validation.Valid;
//import lombok.RequiredArgsConstructor;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@RestController
//@RequiredArgsConstructor
//@RequestMapping("/api/appraisal_reports")
//public class AppraisalReportController {
//
//    private final AppraisalReportService appraisalReportService;
//
//    @GetMapping
//    public ApiResponse<List<AppraisalReportResponseDto>> getAllAppraisalReports(){
//        var reportList = appraisalReportService.getAllAppraisalReports();
//        return new ApiResponse<>(HttpStatus.OK.value(), ApiConstants.RESOURCE_LIST_RETRIEVED, reportList);
//    }
//
//
//    @GetMapping("/{id}")
//    public ApiResponse<AppraisalReportResponseDto> getAppraisalReportById(@PathVariable Long id){
//        return new ApiResponse<>(HttpStatus.OK.value(),
//                ApiConstants.RESOURCE_FOUND,
//                appraisalReportService.getReportById(id));
//    }
//
//    @PostMapping
//    @ResponseStatus(HttpStatus.CREATED)
//    public ApiResponse<AppraisalReportResponseDto> createAppraisalReport(@RequestBody AppraisalReportRequestDto appraisalReportRequestDto) {
//        return new ApiResponse<>(HttpStatus.CREATED.value(),
//                ApiConstants.RESOURCE_CREATED,
//                appraisalReportService.create(appraisalReportRequestDto));
//    }
//
//    @PutMapping("/{id}")
//    @ResponseStatus(HttpStatus.OK)
//    public ApiResponse<AppraisalReportResponseDto> updateAppraisalReport(@PathVariable Long id, @Valid @RequestBody AppraisalReportRequestDto request) {
//        return new ApiResponse<>(
//                HttpStatus.OK.value(),
//                ApiConstants.RESOURCE_UPDATED,
//                appraisalReportService.update(id, request));
//    }
//
//    @DeleteMapping("/{id}")
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    public ApiResponse<AppraisalReportResponseDto> deleteAppraisalReport(@PathVariable Long id) {
//        appraisalReportService.deleteReport(id);
//        return new ApiResponse<>(
//                HttpStatus.NO_CONTENT.value(),
//                ApiConstants.RESOURCE_DELETED, null);
//    }
//
//}
