package com.org.app.controllers;

import com.org.app.constants.ApiConstants;
import com.org.app.dtos.response.ApiResponse;
import com.org.app.dtos.transactionDto.TransactionRequestDto;
import com.org.app.dtos.transactionDto.TransactionResponseDto;
import com.org.app.services.TransactionService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/transactions")
public class TransactionController {
    private final TransactionService transactionService;

    @GetMapping
    public List<TransactionResponseDto> getAllTransactions(){
        return transactionService.findAll();
    }

    @PostMapping
    public ApiResponse<TransactionResponseDto> createNewTransaction(@Valid @RequestBody TransactionRequestDto request) {
        TransactionResponseDto createdTransaction = transactionService.create(request);
        return new ApiResponse<>(
                HttpStatus.CREATED.value(),
                ApiConstants.RESOURCE_CREATED,
                createdTransaction
        );
    }

    @GetMapping("/{id}")
    public ApiResponse<TransactionResponseDto> getTransactionById(@PathVariable Long id){
        return new ApiResponse<>(HttpStatus.OK.value(),
                ApiConstants.RESOURCE_FOUND,
                transactionService.getTransactionById(id));
    }

    @DeleteMapping("/{id}")
    public ApiResponse<TransactionResponseDto> deleteTransaction(@PathVariable Long id) {
        transactionService.deleteTransaction(id);
        return new ApiResponse<>(
                HttpStatus.OK.value(),
                ApiConstants.RESOURCE_DELETED,null);
    }
}
