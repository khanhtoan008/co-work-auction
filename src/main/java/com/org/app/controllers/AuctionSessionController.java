package com.org.app.controllers;


import com.org.app.constants.ApiConstants;
import com.org.app.dtos.auctionDto.AuctionSessionRequestDto;
import com.org.app.dtos.auctionDto.AuctionSessionResponseDto;
import com.org.app.dtos.response.ApiResponse;
import com.org.app.services.AuctionSessionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auction-sessions")
public class AuctionSessionController {

    private final AuctionSessionService auctionSessionService;

    @GetMapping
    public ApiResponse<List<AuctionSessionResponseDto>> getAllAuctionSessions() {
        var auctionSessions = auctionSessionService.getAllAuctionSessions();
        return new ApiResponse<>(HttpStatus.OK.value(),
                ApiConstants.RESOURCE_LIST_RETRIEVED,
                auctionSessions);
    }

    @GetMapping("/{id}")
    public ApiResponse<AuctionSessionResponseDto> getAuctionSessionById(@PathVariable Long id) {
        return new ApiResponse<>(HttpStatus.OK.value(),
                ApiConstants.RESOURCE_FOUND,
                auctionSessionService.getAuctionSessionById(id));
    }

//    @PreAuthorize("hasRole('SELLER')")
    @PostMapping
    public ApiResponse<AuctionSessionResponseDto> createAuctionSession(@RequestBody AuctionSessionRequestDto auctionSession) {
        return new ApiResponse<>(HttpStatus.OK.value(),
                ApiConstants.RESOURCE_CREATED,
                auctionSessionService.createAuctionSession(auctionSession));
    }

////    @PreAuthorize("hasRole('SELLER')")
    @PutMapping("/{id}")
    public ApiResponse<AuctionSessionResponseDto> updateAuctionSession(@PathVariable Long id, @RequestBody AuctionSessionRequestDto auctionSessionDetails) {
        return new ApiResponse<>(
                HttpStatus.OK.value(),
                ApiConstants.RESOURCE_UPDATED,
                auctionSessionService.updateAuctionSession(id, auctionSessionDetails));
    }
//
////    @PreAuthorize("hasRole('SELLER')")
    @DeleteMapping("/{id}")
    public ApiResponse<Void> deleteAuctionSession(@PathVariable Long id) {
        auctionSessionService.deleteAuctionSession(id);
        return new ApiResponse<>(
                HttpStatus.OK.value(),
                ApiConstants.RESOURCE_DELETED,
                null);
    }
}
