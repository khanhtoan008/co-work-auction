package com.org.app.controllers;

import com.org.app.constants.ApiConstants;
import com.org.app.dtos.response.ApiResponse;
import com.org.app.dtos.userDto.UserRequestDto;
import com.org.app.dtos.userDto.UserResponseDto;
import com.org.app.services.UserService;
import com.org.app.utils.SecurityUtils;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/users")
public class UserController {


    private final UserService userService;

    @GetMapping
    public List<UserResponseDto> getAllUsers() {
        return userService.findAllUsers();
    }

    @PostMapping
    public ApiResponse<UserResponseDto> createNewUser(@Valid @RequestBody UserRequestDto request) {
        UserResponseDto createdUser = userService.createUser(request);
        return new ApiResponse<>(
                HttpStatus.CREATED.value(),
                ApiConstants.RESOURCE_CREATED,
                createdUser
        );
    }

    @GetMapping("/{id}")
    public ApiResponse<UserResponseDto> getUserById(@PathVariable Long id) {
        return new ApiResponse<>(HttpStatus.OK.value(),
                ApiConstants.RESOURCE_FOUND,
                userService.getUserById(id));
    }

    @DeleteMapping("/{id}")
    public ApiResponse<UserResponseDto> deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
        return new ApiResponse<>(
                HttpStatus.OK.value(),
                ApiConstants.RESOURCE_DELETED, null);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    public ApiResponse<UserResponseDto> updateProduct(@PathVariable Long id, @Valid @RequestBody UserRequestDto request) {
        return new ApiResponse<>(
                HttpStatus.CREATED.value(),
                ApiConstants.RESOURCE_CREATED,
                userService.updateUser(id, request)
        );
    }

    @GetMapping("/me")
    public String getCurrentUserEmail() {
        return "Id: " + SecurityUtils.getCurrentUserId();
    }

    @GetMapping("/isAuthenticated")
    public boolean isAuthenticated() {
        return SecurityUtils.isAuthenticated();
    }

    @PreAuthorize("#userId ==  authentication.principal.user.id")
    @DeleteMapping("/delete/{userId}")
    public String delete(@PathVariable Long userId) {
        return "Deleted by Id: " + userId;
    }
}
