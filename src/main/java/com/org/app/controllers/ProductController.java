package com.org.app.controllers;

import com.org.app.constants.ApiConstants;
import com.org.app.dtos.productDto.ProductRequestDto;
import com.org.app.dtos.productDto.ProductResponseDto;
import com.org.app.dtos.response.ApiResponse;
import com.org.app.services.ProductService;
import com.org.app.services.azure.AzureBlobService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/products")
public class ProductController {

    private final ProductService productService;
    private final AzureBlobService azureBlobStorageService;

    @GetMapping
    public ApiResponse<List<ProductResponseDto>> getAllProducts() {
        var productLists = productService.getAllProducts();
        return new ApiResponse<>(HttpStatus.OK.value(), ApiConstants.RESOURCE_LIST_RETRIEVED, productLists);
    }


    @GetMapping("/mine")
    public ApiResponse<List<ProductResponseDto>> getMyProducts() {
        List<ProductResponseDto> products = productService.getProductsByCurrentUser();

//        Long currentUserId = SecurityUtils.getCurrentUserId();
//        boolean isAuthenticated = SecurityUtils.isAuthenticated();

        return new ApiResponse<>(
                HttpStatus.OK.value(),
                ApiConstants.RESOURCE_FOUND,
                products
        );
    }

    @GetMapping("/{id}")
    public ApiResponse<ProductResponseDto> getProductById(@PathVariable Long id) {
        ProductResponseDto product = productService.getProductById(id);
        return new ApiResponse<>(
                HttpStatus.OK.value(),
                ApiConstants.RESOURCE_FOUND,
                product
        );
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping()
    public ApiResponse<ProductResponseDto> createProduct(@Valid @RequestBody ProductRequestDto product) {
        ProductResponseDto createdProduct = productService.createProduct(product);
        return new ApiResponse<>(
                HttpStatus.CREATED.value(),
                ApiConstants.RESOURCE_CREATED,
                createdProduct
        );
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    public ApiResponse<ProductResponseDto> updateProduct(@PathVariable Long id, @Valid @RequestBody ProductRequestDto request) {
        return new ApiResponse<>(
                HttpStatus.CREATED.value(),
                ApiConstants.RESOURCE_CREATED,
                productService.updateProduct(id, request)
        );
    }

    @PostMapping("/{productId}/images")
    public ApiResponse<List<String>> uploadProductImages(
            @PathVariable Long productId,
            @RequestParam("images") List<MultipartFile> images) throws IOException {
        var image_of_product = productService.getProductById(productId).getImageUrls().size();
        if (images.size() + image_of_product > 4) {
            return new ApiResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), "You can have a maximum of 4 images of a product.", null);
//            throw new IllegalArgumentException("You can have a maximum of 4 images of a product.");
        }
        List<String> imageUrls = azureBlobStorageService.uploadFiles(images);
        productService.addImagesToProduct(productId, imageUrls);
        return new ApiResponse<>(
                HttpStatus.OK.value(),
                "Images uploaded successfully",
                imageUrls
        );
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/{id}")
    public ApiResponse<?> deleteProduct(@PathVariable Long id) throws IOException {
        productService.deleteProduct(id);

        return new ApiResponse<>(
                HttpStatus.OK.value(),
                ApiConstants.RESOURCE_DELETED, null);
    }
}
