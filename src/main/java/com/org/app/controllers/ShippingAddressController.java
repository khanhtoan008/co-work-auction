package com.org.app.controllers;

import com.org.app.constants.ApiConstants;
import com.org.app.dtos.response.ApiResponse;
import com.org.app.dtos.shippingAddressDto.ShippingAddressDto;
import com.org.app.services.ShippingAddressService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/shipping-addresses")
public class ShippingAddressController {

    private final ShippingAddressService shippingAddressService;

    @GetMapping
    public ApiResponse<List<ShippingAddressDto>> getAllShippingAddresses() {
        var addressList = shippingAddressService.findAll();
        return new ApiResponse<>(HttpStatus.OK.value(), ApiConstants.RESOURCE_LIST_RETRIEVED, addressList);
    }

    @GetMapping("{id}")
    public ApiResponse<ShippingAddressDto> getShippingAddressById(@PathVariable Long id) {

        final var address = shippingAddressService.getShippingAddressById(id);
        return new ApiResponse<>(HttpStatus.OK.value(), ApiConstants.RESOURCE_FOUND, address);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponse<ShippingAddressDto> createNewShippingAddress(@Valid @RequestBody ShippingAddressDto request) {

        final var createdAddress = shippingAddressService.create(request);
        return new ApiResponse<>(HttpStatus.CREATED.value(), ApiConstants.RESOURCE_CREATED, createdAddress);
    }

    @PutMapping("{id}")
    public ApiResponse<ShippingAddressDto> update(@PathVariable Long id, @Valid @RequestBody ShippingAddressDto request) {

        final var updatedAddress = shippingAddressService.updateShippingAddress(id, request);
        return new ApiResponse<>(HttpStatus.OK.value(), ApiConstants.RESOURCE_UPDATED, updatedAddress);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ApiResponse<?> delete(@PathVariable Long id) {

        shippingAddressService.deleteShippingAddressById(id);
        return new ApiResponse<>(HttpStatus.NO_CONTENT.value(), ApiConstants.RESOURCE_DELETED, null);
    }


}
