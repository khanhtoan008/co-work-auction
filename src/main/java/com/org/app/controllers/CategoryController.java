package com.org.app.controllers;

import com.org.app.constants.ApiConstants;
import com.org.app.dtos.categoryDto.CategoryDto;
import com.org.app.dtos.response.ApiResponse;
import com.org.app.services.CategoryService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/categories")
public class CategoryController {

    private final CategoryService categoryService;

    @GetMapping
    public List<CategoryDto> getAllCategories(){
        return categoryService.getAllCategories();
    }


    @GetMapping("/{id}")
    public ApiResponse<CategoryDto> getCategoryById(@PathVariable Long id){
        return new ApiResponse<>(HttpStatus.OK.value(),
                ApiConstants.RESOURCE_FOUND,
                categoryService.getCategoryById(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponse<CategoryDto> createCategory(@RequestBody CategoryDto categoryRequestDto) {
        return new ApiResponse<>(HttpStatus.CREATED.value(),
                ApiConstants.RESOURCE_CREATED,
                categoryService.createCategory(categoryRequestDto));
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ApiResponse<CategoryDto> updateCategory(@PathVariable Long id, @Valid @RequestBody CategoryDto categoryDetails) {
        return new ApiResponse<>(
                HttpStatus.OK.value(),
                ApiConstants.RESOURCE_UPDATED,
                categoryService.updateCategory(id, categoryDetails));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ApiResponse<CategoryDto> deleteCategory(@PathVariable Long id) {
        categoryService.deleteCategory(id);
        return new ApiResponse<>(
                HttpStatus.NO_CONTENT.value(),
                ApiConstants.RESOURCE_DELETED, null);
    }
}
