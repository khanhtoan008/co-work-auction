CREATE TABLE appraisal_reports
(
    id           BIGINT AUTO_INCREMENT NOT NULL,
    created_time datetime              NULL,
    updated_time datetime              NULL,
    appraisal_id BIGINT                NULL,
    detail       VARCHAR(255)          NULL,
    status       BIT(1)                NULL,
    CONSTRAINT pk_appraisal_reports PRIMARY KEY (id)
);

CREATE TABLE auction_sessions
(
    id            BIGINT AUTO_INCREMENT NOT NULL,
    created_time  datetime              NULL,
    updated_time  datetime              NULL,
    current_price FLOAT                 NULL,
    duration      VARCHAR(255)          NULL,
    sellers_id    BIGINT                NULL,
    product_id    BIGINT                NULL,
    start_time    datetime              NULL,
    CONSTRAINT pk_auction_sessions PRIMARY KEY (id)
);

CREATE TABLE auction_sessions_buyers
(
    buyers_id                  BIGINT NOT NULL,
    joined_auction_sessions_id BIGINT NOT NULL
);

CREATE TABLE bids
(
    id           BIGINT AUTO_INCREMENT NOT NULL,
    created_time datetime              NULL,
    updated_time datetime              NULL,
    user_id      BIGINT                NULL,
    bid_amount   FLOAT                 NULL,
    CONSTRAINT pk_bids PRIMARY KEY (id)
);

CREATE TABLE categories
(
    id            BIGINT AUTO_INCREMENT NOT NULL,
    created_time  datetime              NULL,
    updated_time  datetime              NULL,
    name          VARCHAR(255)          NULL,
    `description` VARCHAR(255)          NULL,
    CONSTRAINT pk_categories PRIMARY KEY (id)
);

CREATE TABLE orders
(
    id             BIGINT AUTO_INCREMENT NOT NULL,
    created_time   datetime              NULL,
    updated_time   datetime              NULL,
    shipping_code  VARCHAR(255)          NULL,
    total          INT                   NOT NULL,
    status         BIT(1)                NULL,
    product_id     BIGINT                NULL,
    transaction_id BIGINT                NULL,
    CONSTRAINT pk_orders PRIMARY KEY (id)
);

CREATE TABLE products
(
    id                   BIGINT AUTO_INCREMENT NOT NULL,
    created_time         datetime              NULL,
    updated_time         datetime              NULL,
    name                 VARCHAR(255)          NULL,
    image                VARCHAR(255)          NULL,
    `description`        VARCHAR(255)          NULL,
    buyout_price         FLOAT                 NULL,
    start_price          FLOAT                 NULL,
    owner_id             BIGINT                NULL,
    category_id          BIGINT                NULL,
    appraisal_reports_id BIGINT                NULL,
    CONSTRAINT pk_products PRIMARY KEY (id)
);

CREATE TABLE roles
(
    id           BIGINT AUTO_INCREMENT NOT NULL,
    created_time datetime              NULL,
    updated_time datetime              NULL,
    name         VARCHAR(255)          NULL,
    CONSTRAINT pk_roles PRIMARY KEY (id)
);

CREATE TABLE shipping_addresses
(
    id           BIGINT AUTO_INCREMENT NOT NULL,
    created_time datetime              NULL,
    updated_time datetime              NULL,
    street       VARCHAR(255)          NULL,
    ward         VARCHAR(255)          NULL,
    district     VARCHAR(255)          NULL,
    city         VARCHAR(255)          NULL,
    CONSTRAINT pk_shipping_addresses PRIMARY KEY (id)
);

CREATE TABLE transactions
(
    id           BIGINT AUTO_INCREMENT NOT NULL,
    created_time datetime              NULL,
    updated_time datetime              NULL,
    wallet_id    BIGINT                NULL,
    type         VARCHAR(255)          NULL,
    amount       FLOAT                 NULL,
    time         datetime              NULL,
    status       VARCHAR(255)          NULL,
    order_id     BIGINT                NULL,
    CONSTRAINT pk_transactions PRIMARY KEY (id)
);

CREATE TABLE users
(
    id           BIGINT AUTO_INCREMENT NOT NULL,
    created_time datetime              NULL,
    updated_time datetime              NULL,
    name         VARCHAR(255)          NULL,
    email        VARCHAR(255)          NULL,
    password     VARCHAR(255)          NULL,
    cccd         VARCHAR(255)          NULL,
    rate         FLOAT                 NULL,
    role_id      BIGINT                NULL,
    CONSTRAINT pk_users PRIMARY KEY (id)
);

CREATE TABLE wallets
(
    id           BIGINT AUTO_INCREMENT NOT NULL,
    created_time datetime              NULL,
    updated_time datetime              NULL,
    balance      FLOAT                 NULL,
    user_id      BIGINT                NULL,
    CONSTRAINT pk_wallets PRIMARY KEY (id)
);

ALTER TABLE users
    ADD CONSTRAINT uc_users_cccd UNIQUE (cccd);

ALTER TABLE appraisal_reports
    ADD CONSTRAINT FK_APPRAISAL_REPORTS_ON_APPRAISAL FOREIGN KEY (appraisal_id) REFERENCES users (id);

ALTER TABLE auction_sessions
    ADD CONSTRAINT FK_AUCTION_SESSIONS_ON_PRODUCT FOREIGN KEY (product_id) REFERENCES products (id);

ALTER TABLE auction_sessions
    ADD CONSTRAINT FK_AUCTION_SESSIONS_ON_SELLERS FOREIGN KEY (sellers_id) REFERENCES users (id);

ALTER TABLE bids
    ADD CONSTRAINT FK_BIDS_ON_USER FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE orders
    ADD CONSTRAINT FK_ORDERS_ON_PRODUCT FOREIGN KEY (product_id) REFERENCES products (id);

ALTER TABLE orders
    ADD CONSTRAINT FK_ORDERS_ON_TRANSACTION FOREIGN KEY (transaction_id) REFERENCES transactions (id);

ALTER TABLE products
    ADD CONSTRAINT FK_PRODUCTS_ON_APPRAISALREPORTS FOREIGN KEY (appraisal_reports_id) REFERENCES appraisal_reports (id);

ALTER TABLE products
    ADD CONSTRAINT FK_PRODUCTS_ON_CATEGORY FOREIGN KEY (category_id) REFERENCES categories (id);

ALTER TABLE products
    ADD CONSTRAINT FK_PRODUCTS_ON_OWNER FOREIGN KEY (owner_id) REFERENCES users (id);

ALTER TABLE transactions
    ADD CONSTRAINT FK_TRANSACTIONS_ON_ORDER FOREIGN KEY (order_id) REFERENCES orders (id);

ALTER TABLE transactions
    ADD CONSTRAINT FK_TRANSACTIONS_ON_WALLET FOREIGN KEY (wallet_id) REFERENCES wallets (id);

ALTER TABLE users
    ADD CONSTRAINT FK_USERS_ON_ROLE FOREIGN KEY (role_id) REFERENCES roles (id);

ALTER TABLE wallets
    ADD CONSTRAINT FK_WALLETS_ON_USER FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE auction_sessions_buyers
    ADD CONSTRAINT fk_aucsesbuy_on_auction_session FOREIGN KEY (joined_auction_sessions_id) REFERENCES auction_sessions (id);

ALTER TABLE auction_sessions_buyers
    ADD CONSTRAINT fk_aucsesbuy_on_user FOREIGN KEY (buyers_id) REFERENCES users (id);